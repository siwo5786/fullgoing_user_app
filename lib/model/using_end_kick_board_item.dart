class UsingEndKickBoardItem {
  String dateEnd;
  String dateStart;
  String kickBoardName;
  num resultPass;
  num resultPrice;

  UsingEndKickBoardItem(
      this.dateEnd,
      this.dateStart,
      this.kickBoardName,
      this.resultPass,
      this.resultPrice,
      );

  factory UsingEndKickBoardItem.fromJson(Map<String, dynamic> json) {
    return UsingEndKickBoardItem(
      json['dateEnd'],
      json['dateStart'],
      json['kickBoardName'],
      json['resultPass'],
      json['resultPrice'],
    );
  }
}
